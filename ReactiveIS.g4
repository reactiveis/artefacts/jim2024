/*
 * =====================
 * Syntax of ReactiveIS
 * =====================

 * initial production : structure
 * Some definitions to be taken into account:
 *  - Structure : a TO (temporal object) without start/stop messages but with
 *                start/stop conditions
 *  - Texture   : a TO with start/stop messages (and conditions)
 *  - Event     : a TO with only start message and condition  (a texture without duration)
 *
 * The keyword event is used for describing a TO. The keyword ev is used to
 * denote the events (start/stop) that can be used for defining conditions.
 *
 * Loops and conditional are considered also as temporal objects in the sense
 * that they need a condition to "start" (and evaluate the condition)
 */

grammar ReactiveIS;

// -----------------------------------
// Keywords and tokens
// -----------------------------------

TEXTURE           : 'texture'      ;
STRUCTURE         : 'structure'    ;
EVENT             : 'event'        ;
START             : 'start'        ;
STOP              : 'stop'         ;
WAIT              : 'wait'         ;
EV                : 'ev'           ;
CONDITIONAL       : 'conditional'  ;
LOOP              : 'loop'         ;

// Attributes
NAME              : 'name'         ;
CSTART            : 'start-when'   ;
CSTOP             : 'stop-when'    ;
TOS               : 'content'      ;
MSTART            : 'msg-start'    ;
MSTOP             : 'msg-stop'     ;
CHECK             : 'check'        ;
TOTRUE            : 'cont-true'    ;
TOFALSE           : 'cont-false'    ;

// Boolean expressions
TRUE              : 'True'         ;
FALSE             : 'False'        ;
CONJ              : '/\\'          ;
DISJ              : '\\/'          ;

//Infinite token
INF               : 'INF' ;

// Relational expressions
EQ                : '='            ;
NEQ               : '<>'           ;
RELSYM            : '<' | '>'
                    | '>=' | '<='  ;

// Miscellaneous
SEP               : ',' ;
WHITESPACE        : (' ' | '\t')+ -> skip  ;
NEWLINE           : ('\r'? '\n' | '\r')+ -> skip ;
ID                : '_'* [A-Za-z][A-Za-z0-9_]* ;
NAT               : [0-9]+ ;
TEXT              : '"'  ~('"')* '"' ;
COMMENT           : ('#' .*? ('#' | '\n' )) -> skip ;
OPEN              : '{' ;
CLOSE             : '}';
COLON             : ':' ;
SCOLON            : ';' ;
QUOTE             : '"' ;
OBRACKET          : '[' ;
CBRACKET          : ']' ;
OPAR              : '(' ;
CPAR              : ')' ;


// --------------------
// Grammar
// --------------------

score : structure
      ;

// --------------------
// Temporal objects
// --------------------

// TO (Temporal Objects)
tobj   :   structure
          | texture
          | event
          | conditional
          | loop
          ;

structure : STRUCTURE COLON ID
            OPEN
                name
                cstart
                cstop
                tos
            CLOSE
            ;


event :   EVENT COLON ID
            OPEN
                name
                cstart
                mstart
            CLOSE
            ;


texture : TEXTURE COLON ID
            OPEN
                name
                cstart
                cstop
                mstart
                mstop
            CLOSE
            ;

conditional : CONDITIONAL COLON ID
              OPEN
                name
                cstart
                check
                ctrue
                cfalse?
              CLOSE
              ;

loop        : LOOP  COLON ID
              OPEN
                name
                cstart
                check
                ctrue
              CLOSE
              ;

// ------------
// Attributes
// ------------

name   : NAME     COLON TEXT SCOLON ;
cstart : CSTART   COLON condition SCOLON ;
cstop  : CSTOP    COLON condition SCOLON ;
mstart : MSTART   COLON TEXT SCOLON ;
mstop  : MSTOP    COLON TEXT SCOLON ;
tos    : TOS      COLON OBRACKET tobj* CBRACKET SCOLON ;
check  : CHECK    COLON condparameter SCOLON ;
ctrue  : TOTRUE   COLON tobj SCOLON ;
cfalse : TOFALSE  COLON tobj SCOLON ;

// ----------------
// Conditions
// ----------------
condition :
         wait_exp                                                      # Wait
       | OPAR inner=condition CPAR                                     # Parentheses
       | left=condition operator=(CONJ | DISJ)    right=condition      # BoolExpr
       | ( TRUE | FALSE)                                               # BoolCte
       ;

// Conditions on parameter values
condparameter :
        TEXT oprel NAT ;

oprel :  EQ | NEQ | RELSYM ;


wait_exp : WAIT OPAR ev_wait CPAR ;

// External and internal events
ev_wait  : external_ev | internal_ev ;

// External events with possible values
external_ev : EV
               OPAR
                 TEXT
               CPAR
               ;

internal_ev : (START | STOP)
                   OPAR ID  CPAR
                         (SEP NAT (SEP (NAT | INF))?)?     // Interval
                   ;
