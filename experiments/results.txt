PRELIMINARIES:
- The vertical lines are called "Time Sync" (TC)
- The horizontal lines are intervals 

===================================================================
Experiment 1:
===================================================================
Elements: 
- 3 incoming intervals named i1, i2, i3 from top to bottom
- A TC with a trigger
-----------------------------------------------------------------------------------------------
Results:
- There is a TS with a trigger and all the incoming intervals have 
  time relations (min,max). The execution of the outgoing interval
  is done when: 
    * The trigger is executed between the minimum of all maximum of all the 
    min values and the minimum of all max values of the time relations
    OR
    * The execution time reaches the minimum of all the max values 
      (in this case the max of i1)

===================================================================
Experiment 2:
===================================================================
Elements: 
- 2 incoming intervals named i1, i2 from top to bottom. In this case
  i1 can be desynchronized, due to the first trigger.
- 1 outgoing interval named OI
- 2 TC TC1, TC2 from left to right with triggers 
-----------------------------------------------------------------------------------------------
Results:
- When the trigger in TC1 is not activated, then i1 is not executed, i2 reaches it max and OI is 
  not executed
- When the trigger in TC1 is activated, then i1 is executed. IO is executed when one of the
  following conditions is satisfied first, WHEN BOTH INTERVALS HAVE REACHED AT LEAST THEIR MIN VALUE:
    * interval i1 gets to it's max
    * interval i2 gets to it's max
    * The triggered is activated 

===================================================================
Experiment 3:
===================================================================
Elements:
- Experiment 3 is basically experiment 2 but with an additional incoming interval i3, that
  doesn't has a time relation (it doesn't has an interval (max,min))
-----------------------------------------------------------------------------------------------
Results: 
- When the trigger in TC1 is activated, then i1 is executed. IO is executed when one of the
  following conditions is satisfied first, WHEN BOTH INTERVALS HAVE REACHED AT LEAST THEIR MIN VALUE:
    * interval i1 gets to it's max
    * interval i2 gets to it's max
    * The triggered is activated
    * interval i3 gets completes
- In this case, IO will be executed when interval i3 completes, since it doesn't
  has a min or max it will be the first one to execute of the 3 intervals

===================================================================
Experiment 4:
===================================================================
Elements:
- 2 incoming intervals named i1, i2 from top to bottom. In this case
  i1 can be desynchronized, due to the first trigger.
- 1 outgoing interval named OI
- 2 TC TC1, TC2 from left to right with triggers
-----------------------------------------------------------------------------------------------
Results: 
- When the trigger in TC1 is not activated, then i1 is not executed, i2 reaches TC2 and OI is 
  not executed
- When the trigger in TC1 is activated, then i1 is executed. IO is executed when bot i1 and i2
  get to TC2 


===================================================================
CONCLUSIONS:
===================================================================
Based on the results of these 4 experiments and many more undocumented ones, the following model
encapsulates how TCs work in Ossia: 
-----------------------------------------------------------------------------------------------
Definitions:
- Intervals: we will represent intervals by an inteval identifier, a min value and a max value
  (time relation), and a duration. Therefore an interval i1 with min value x, max value y and 
  duration d will be represented as i1(x,y,d) and i1[0] = x, i1[1] = y, i1[2] = d. Intervals 
  that appear with no time relation in Ossia (intervals like this one o-----------o ) are 
  represented as i(d,0,d).

- TCs: TCs will be represented by a TC identifier, a trigger identifier, a set of incoming intervals
  identifiers and a set of outgoing intervals identifiers. For example the second TC in experiment 2 would be 
  TC2(TRG1,{i1,i2},{OI}) and TC[0] = TRG1, TC[1] = {i1,i2}, TC[1] = {OI}. In case that the TC doesn't has a 
  trigger, then TC[0] = None.  

- inReady(i,t): A function that recieves an interval identifier i, a time value t and returns a boolean 
  value. It verifies if the  interval i has been executed until its min value in the current time t, 
  i.e. the "gree bar" (the moving bar when a execution is runned) has reached its min value at time t. Therefore:
    * inReady(i,t) : IF ("greenbar" >= i[0]) THEN true ELSE false.

- inMax(i,t): The same definition of inReady, but with the max value: 
	* inMax(i,t) : IF ("greenbar" >= i[1]) THEN true ELSE false.
	
- exec(trg): A function that recieves a trigger identifier and simulates the execution of it

- inEnabled(i,t): A function that takes an interval identifier i, a time value t and returns a boolena
  value. It verifies if the interval i is enabled (it will be executed) or is disabled(it will not be 
  executed, due to a previous conditional not being satisfied). enabled = true, disabled = false.
-----------------------------------------------------------------------------------------------
TC Function:
The following function defines when a TC will "execute" its outgoing intervals. It takes
a TC, the current time t and returns true if the TC will execute or false if it wont:

execTC(TC,t):
  
  enabled = {} #set of interval identifiers
  
  FOR interval in TC[1]:
    IF inEnabled(interval,t):
      enabled = enabled U {interval}

  IF (forall interval in enabled, inReady(interval) = true):

    IF TC[0] =/= None:
      IF exec(TC[0]):
        return true

    FOR interval in TC[1]:
      IF inMax(interval,t):
        return true
  ELSE:
    return false
-----------------------------------------------------------------------------------------------
Generally speaking, Ossia executes a TC when:
	* The greenbar reaches the min value of all ENABLED incoming intervals:
	AND 
	[(* there is a trigger and it is executed) 
	OR 
	(* one of its incoming intervals gets to its max value)]

	P1 AND [(P2) OR (P3)]