#!/usr/bin/env python
"""
A parser from ReactiveIS to Maude
"""

import argparse
import traceback

from antlr4 import *

from dist.ReactiveISLexer import ReactiveISLexer
from dist.ReactiveISListener import ReactiveISListener
from dist.ReactiveISParser import ReactiveISParser


class ISMaude(ReactiveISListener):
    """Parser related functions"""

    def __init__(self, output):
        """output stream"""
        self._output = output
        # output string
        self.answer = ""
        # auxiliary indent string
        self.indent = "                 "

    def parseExternalEvent(self, ctx: ReactiveISParser.External_evContext):
        """Parse external_ev rule"""

        string = "event(" + ctx.TEXT().getText() + ")"
        return string

    def parseInternalEvent(self, ctx: ReactiveISParser.Internal_evContext):
        """Parse internal_ev rule"""

        string = ""

        # determine if it is start or stop
        if ctx.START():
            string += "start('"
        elif ctx.STOP():
            string += "stop('"
        else:
            raise Exception("Error in method parseInternalEvent")

        # add ID
        string += ctx.ID().getText() + ")"

        # get number of NAT and add them to the string
        NATs = ctx.NAT()
        number = len(NATs)
        # verify if there is an INF token
        if ctx.INF():
            string += "," + NATs[0].getText() + ",inf"
        else:
            if number == 1:
                string += "," + NATs[0].getText()
            elif number == 2:
                string += "," + NATs[0].getText() + "," + NATs[1].getText()
            else:
                raise Exception("Error in method parseInternalEvent")

        return string

    def parseEvWait(self, ctx: ReactiveISParser.Ev_waitContext):
        """Parse ev_wait rule"""

        string = ""
        # verify if it is external or internal
        if ctx.external_ev():
            string = self.parseExternalEvent(ctx.external_ev())
        elif ctx.internal_ev():
            string = self.parseInternalEvent(ctx.internal_ev())
        else:
            raise Exception("Error in method parseEvWait")

        return string

    def parseWaitExp(self, ctx: ReactiveISParser.Wait_expContext):
        """Parse wait_exp rule"""

        string = "wait(" + self.parseEvWait(ctx.ev_wait()) + ")"
        return string

    def parseCondParam(self, ctx: ReactiveISParser.CondparameterContext):
        """
        TODO: Parse condparameter rule. For now this method will not be
        implemented, since the content of the condition for verification is
        irrelevant.
        """
        pass

    def parseCondition(self, ctx: ReactiveISParser.ConditionContext):
        """Parse condition rule"""

        string = ""

        # check which case of the condition rule is
        contextType = type(ctx)

        # case Wait
        if contextType == ReactiveISParser.WaitContext:
            string = self.parseWaitExp(ctx.wait_exp())

        # case Parentheses
        elif contextType == ReactiveISParser.ParenthesesContext:
            string = "(" + self.parseCondition(ctx.inner) + ")"

        # case BoolExpr
        elif contextType == ReactiveISParser.BoolExprContext:
            # check if it is '/\' or '\/'
            symbol = ""
            if ctx.CONJ():
                symbol = " AND "
            elif ctx.DISJ():
                symbol = " OR "
            else:
                raise Exception("Error in method parseCondition")

            string = (
                self.parseCondition(ctx.left) + symbol + self.parseCondition(ctx.right)
            )

        # case BoolCte
        elif contextType == ReactiveISParser.BoolCteContext:
            # True and False have the same syntax in Maude
            string = ctx.getText()

        else:
            raise Exception("Error in method parseCondition")

        return string

    def parseStructure(self, ctx: ReactiveISParser.StructureContext):
        """Parse structure rule"""

        string = ""

        # list of TO's IDs inside the structure
        ids = list()

        # define structure attributes as parsed strings
        oid = "'" + ctx.ID().getText()
        startCondition = self.parseCondition(ctx.cstart().condition())
        stopCondition = self.parseCondition(ctx.cstop().condition())

        # get list of child TOs
        TOS = ctx.tos().tobj()

        # get childs id's
        for to in TOS:
            # check which case of the structure rule is and add id of child to the ids list
            if to.structure():
                ids.append("'" + to.structure().ID().getText())
            elif to.texture():
                ids.append("'" + to.texture().ID().getText())
            elif to.event():
                ids.append("'" + to.event().ID().getText())
            elif to.conditional():
                ids.append("'" + to.conditional().ID().getText())
            elif to.loop():
                ids.append("'" + to.loop().ID().getText())

        # construct the parent structure as string
        string = "make-structure(" + oid + ", "

        # if there are no child elements, use empty
        if len(TOS) == 0:
            string += "empty, "
        else:
            string += "("
            # add to string the list of childs
            for i in range(len(ids) - 1):
                string += ids[i] + ", "
            string += ids[len(ids) - 1] + "), "

        # add conditions
        string += startCondition + ", " + stopCondition + ")\n" + self.indent

        return string

    def parseTexture(self, ctx: ReactiveISParser.TextureContext):
        """Parse texture rule"""

        string = ""

        # define texture attributes as parsed strings
        oid = "'" + ctx.ID().getText()
        startCondition = self.parseCondition(ctx.cstart().condition())
        stopCondition = self.parseCondition(ctx.cstop().condition())
        mstart = "msg(" + ctx.mstart().TEXT().getText() + ")"
        mstop = "msg(" + ctx.mstop().TEXT().getText() + ")"

        string = (
            "make-texture("
            + oid
            + ", "
            + startCondition
            + ", "
            + stopCondition
            + ", "
            + mstart
            + ", "
            + mstop
            + ")\n"
            + self.indent
        )

        return string

    def parseEvent(self, ctx: ReactiveISParser.EventContext):
        """Parse event rule"""

        string = ""

        # define Event attributes as parsed strings
        oid = "'" + ctx.ID().getText()
        startCondition = self.parseCondition(ctx.cstart().condition())
        # Stop condition are True since events have no duration
        stopCondition = "True"
        mstart = "msg(" + ctx.mstart().TEXT().getText() + ")"
        mstop = 'msg("")'

        string = (
            "make-texture("
            + oid
            + ", "
            + startCondition
            + ", "
            + stopCondition
            + ", "
            + mstart
            + ", "
            + mstop
            + ")\n"
            + self.indent
        )

        return string

    def parseConditional(self, ctx: ReactiveISParser.ConditionalContext):
        """Parse conditional rule"""

        string = ""

        # define conditional attributes as parsed strings
        oid = "'" + ctx.ID().getText()
        startCondition = self.parseCondition(ctx.cstart().condition())

        # TO IF condition
        TOIf = ""
        # TO ELSE condition
        TOElse = ""

        to = ctx.ctrue().tobj()
        # check which case of the structure rule is and add id of child to the ids list
        if to.structure():
            TOIf = "'" + to.structure().ID().getText()
        elif to.texture():
            TOIf = "'" + to.texture().ID().getText()
        elif to.event():
            TOIf = "'" + to.event().ID().getText()
        elif to.conditional():
            TOIf = "'" + to.conditional().ID().getText()
        elif to.loop():
            TOIf = "'" + to.loop().ID().getText()

        # check if else is not empty
        if ctx.cfalse():
            to = ctx.cfalse().tobj()
            # check which case of the structure rule is and add id of child to the ids list
            if to.structure():
                TOElse = "'" + to.structure().ID().getText()
            elif to.texture():
                TOElse = "'" + to.texture().ID().getText()
            elif to.event():
                TOElse = "'" + to.event().ID().getText()
            elif to.conditional():
                TOElse = "'" + to.conditional().ID().getText()
            elif to.loop():
                TOElse = "'" + to.loop().ID().getText()
        else:
            TOElse = "empty"

        # make conditional
        string = (
            "make-conditional("
            + oid
            + ", ("
            + TOIf
            + "), ("
            + TOElse
            + "), "
            + startCondition
            + ")\n"
            + self.indent
        )

        return string

    def parseLoop(self, ctx: ReactiveISParser.LoopContext):
        """TODO: Parse loop rule"""
        pass

    def parseTobj(self, ctx: ReactiveISParser.TobjContext):
        """
        Parse all the TOs in the score. The returned string is the configuration
        recieved by Maude eq "make-system". answer is the returned string, saved
        as attribute of class ISMaude.
        """

        # Check the Tobj type
        if ctx.structure():
            self.answer += self.parseStructure(ctx.structure())
            for to in ctx.structure().tos().tobj():
                self.parseTobj(to)

        elif ctx.texture():
            self.answer += self.parseTexture(ctx.texture())

        elif ctx.event():
            self.answer += self.parseEvent(ctx.event())

        elif ctx.conditional():
            self.answer += self.parseConditional(ctx.conditional())
            # parse child element IF
            self.parseTobj(ctx.conditional().ctrue().tobj())
            # parse child element ELSE
            if ctx.conditional().cfalse():
                self.parseTobj(ctx.conditional().cfalse().tobj())

        # not implemented yet
        elif ctx.loop():
            pass

        else:
            raise Exception("Error in method parseTobj")

    def exitScore(self, ctx: ReactiveISParser.ScoreContext):
        """Parse file and write answer in document _output"""

        # maude header file
        header = """load semantics .

mod SCENARIO is
  protecting SEMANTICS .

  --- System's intial state
  op initState : -> Object .
  eq initState = make-system("""

        # add header to answer
        self.answer += header

        # add score ID
        self.answer += "'" + ctx.structure().ID().getText() + ",\n" + self.indent

        self.answer += self.parseStructure(ctx.structure())
        for to in ctx.structure().tos().tobj():
            self.parseTobj(to)

        # add footer to string
        self.answer += ") .\n\nendm"

        # write in file the answer
        self._output.write(self.answer)


def main(fin: str, fout: str):
    """Main entry for the parser:

    Parameters
    ----------
    fin : str
        input file name
    fout : str
        output file name
    """
    try:
        ipt = FileStream(fin)
        lexer = ReactiveISLexer(ipt)
        stream = CommonTokenStream(lexer)
        parser = ReactiveISParser(stream)
        tree = parser.score()  # Initial entry for the grammar
        output = open(fout, "w")
        printer = ISMaude(output)
        walker = ParseTreeWalker()
        walker.walk(printer, tree)
        output.close()
        print(f"output: {fout}")
    except Exception as E:
        print(f"Error processing {fin}.\n {E}")
        traceback.print_exc()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=""" From ReactiveIS to Maude specifications """,
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--output", type=str, default="./out.maude", help="Maude output file"
    )

    parser.add_argument("--input", type=str, required=True, help="ReactiveIS file")
    args = parser.parse_args()
    main(args.input, args.output)
