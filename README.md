# IS2Maude

This repository contains a rewriting logic specification for the specification
and verification of interactive scores.

## Folder Structure

```bash
.
├── IS2Maude.py         # Parse ReactiveIS to Maude
├── Makefile            # Generate grammar and run examples
├── ReactiveIS.g4       # antlr grammar of ReactiveIS
├── examples            # folder containing some reactiveIS examples
├── experiments         # folder containing some experiments
├── maude               # folder containing the maude specification
└── requirements.txt    # requirements to run the IS2Maude.py
```

## Dependencies


The project was tested in [Maude 3.3.1](http://maude.cs.illinois.edu/). The
parser from ReactiveIS files into Maude files was written in
[`Python3`](https://www.python.org/downloads/) and the dependencies can be
installed using the following command.

```bash
pip3 install -r requirements.txt
```

If the parser and lexer files need to be generated, you can run the following
command once you have installed [antlr](https://www.antlr.org/).

```bash
make compile
# or
antlr -Dlanguage=Python3 -listener -visitor -o dist ReactiveIS.g4
```

## Getting Started

`IS2Maude` takes as input a ReactiveIS score and returns a
[`Maude`](http://maude.cs.illinois.edu/w/index.php/The_Maude_System)
specification.

```bash
python3 IS2Maude.py --input ./examples/example.is --output example.maude
```

You can also generate all the example by running:

```bash
make all
```

### Maude files

The specification can be found in the folder `maude`. It includes the following files:

- _reactive-str_: Definition of commands to perform reachability analysis.
- _semantics_: Rewriting rules defining the semantics of ReactiveIS programs
- _syntax_: Syntax for defining ReactiveIS programs.

# Acknowledgements

This work has been partially funded by the APE project (Université Sorbonne Paris Nord) SEMANTIQUE and
the MSH Paris Nord project CORRECTNESS (24 1 A 5). We are grateful to Daniel Osorio for helping us with
the implementation of the parser of REACTIVEIS programs to Maude.