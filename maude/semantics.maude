***(

# A semantics for ReactiveIS in Maude

## Representation:

### State of the system

 The state of a program is represented by a term of the form
    < top : System |  prog:  Sys,            --- system (program)
                      idle:  IDLE,           --- TOs not played
                      run:   RUN,            --- TOs currently running
                      done:  DONE,           --- TOs already played
                      pmsg:  PMSG,           --- Persistent messages
                      in:    IN,             --- Input/external messages
                      out:   OUT,            --- Output (produced) messages
                      time:  T               --- Global clock
                      > .

 The function make-system is useful to build such terms.

### Temporal Object (TOs)

  Qid (quoted identifiers) are used as object (TOs) identifiers.

  Temporal objects are represented as terms of sort Object as follows:

      < O1 : TO | parent: O2,                --- parent node
                  start: C1,            --- start condition
                  stop: C2,             --- stop condition
                  mstart: M1,           --- start message
                  mstop: M2             --- stop message
                  > .

    There are two useful functions for build TOs:
     - make-texture: for building textures (TOs with start/stop messages)
     - make-structure: where the start/stop messages are msg("")

 ### Conditions
   Conditions are built from the following syntax:
   C  ::= True | False | C AND C | C OR C | wait(WC, [n, [m ]])
   WC ::= event(str) | start(TO) | stop(TO)

   The interval [n-m] is optional.  The waiting condition event(str) is meant to
   represent external events; start(TO) and stop(TO) are used for internal
   events.

 ### Messages
   Messages are terms of the sort Msg (defined in the module CONFIGURATION).
   There are three kind of messages:
   - msg(str) representing an external message
   - start(O, t): the internal message, start(O) happened at time t
   - stop(O, t): the internal message, stop(O) happened at time t

## Semantics
 The behavior of the system is specified by the following rules:
 - [tick]: advances the time in one time-unit
 - [start]: starts a TO, moving it from IDLE to RUN
 - [stop] : stops a TO, moving it from RUN to DONE

 The specification of [start] and [stop] requires additional rules governing
 the evaluation of conditions. For that, conditions are enclosed in a context:

  op {T;E;I;C} : Nat Configuration Configuration Condition -> Condition [ctor] .

  where the global time is T, E and I are, respectively, the set of external
  and internal messages and C is the condition being evaluated.

  The evaluation of the boolean connective is the expected one. For external
  and internal events, we have:
  - Rule [EV-EXT] reduces to True if the external message is present. Otherwise,
                  it reduces to False.
  - Rule [EV-INT] reduces to True if the message is in the set of internal
                  message. Otherwise, the evaluation does not progress.
                  This is explained by the fact that other rules may
                  further add new (internal) messages during the current
                  time-unit. Hence, not observing the message "now" does not
                  necessarily imply that it will be produced later (during
                  the same time-unit).

  ### Evaluation Strategy
  The theory is meant to be executed using the following strategy:
  1. normalize the term using [start] and [stop]
  2. advance the time
  3. Check if there are idle/run TOs
  3a. if this is the case, go back to 1.
  3b. ends

## Verifying Properties
- Is there a TO that starts and stops in the same time unit?
- Is there a TO that is never played?
- What is the maximal (absolute) duration of a given TO?
- Can the TOs A and B be executed concurrently?
- Is A played always before B?
- ** Symbolic: synthesis of constraints. What are the constraints that
               allow to play a box.

- TODO
  - If AND/OR are not commutative, then the order of application is unique thus
    reducing the state space (Be careful that, without associativity, the
    evaluation of conditional expressions may block)

- MODIFICATIONS
 - 19/05: Now we have two new states, namely, cant-start and cant-stop to
   store the IDs of the boxes that cannot start/stop in the current time-unit
   This allows for avoiding check twice the same condition during the
   current time-unit

*)

load syntax .

mod SEMANTICS is
  pr SYNTAX .

  --- Identifiers for classes
  op System  :    -> Cid [ctor] .

  --- Top level scenario
  op top : -> Oid .


  --- Messages
  op start : Oid Nat                        -> Msg [ctor] .
  op stop : Oid Nat                         -> Msg [ctor] .

  --- ------------------------------------
  --- Attributes for the top-level object
  --- ------------------------------------

  --- The argument is a collection of objects (representing the boxes)
  op prog:_ : Configuration                 -> Attribute [ctor format(n d d)] .
  --- Not played boxes
  op idle:_ : TOSet                         -> Attribute [ctor format(n d d)] .
  --- TOs currently being played
  op run:_ : TOSet                          -> Attribute [ctor format(n d d)] .
  --- TOs already played
  op done:_ : TOSet                         -> Attribute [ctor format(n d d)] .
  --- TOs that cannot start now
  op cant-start:_ : TOSet                   -> Attribute [ctor format(n d d)] .
  --- TOs that cannot stop now
  op cant-stop:_ : TOSet                    -> Attribute [ctor format(n d d)] .
  --- Set of persistent messages
  op pmsg:_ : Configuration                 -> Attribute [ctor format(n d d)] .
  --- Output and input messages
  op in:_ : Configuration                   -> Attribute [ctor format(n d d)] .
  op out:_ : Configuration                  -> Attribute [ctor format(n d d)] .
  --- Global clock
  op time:_ : Nat                           -> Attribute [ctor format(n d d)] .

  --- -------------------------------------------
  vars Sys Sys'                 : Configuration .
  vars Cnf Cnf'                 : Configuration .
  vars C1 C2 C1' C2'            : Condition .
  vars BC BC'                   : BCondition .
  vars O1 O2 O1' O2'            : Oid .
  vars M1 M2 M1' M2'            : Msg .
  vars SB SB'                   : TOSet .
  vars TOs TOs'                 : TOSet .
  vars IN OUT PMSEG             : Configuration .
  vars IDLE RUN DONE            : TOSet .
  vars NSTART NSTOP             : TOSet .
  vars O O'                     : Object .
  vars ats ats'                 : AttributeSet .
  vars MIN MOUT MEXT            : Configuration .
  vars str str'                 : String .
  vars M M'                     : Msg .
  var n m n' m' T T'            : Nat .
  var WC                        : WaitCondition .
  --- -------------------------------------------

  --- Evaluation context for conditions
  --- parameters: Global time, external and internal messages, condition
  op {_;_;_;_} : Nat Configuration Configuration Condition -> Condition [ctor] .

  --- ------------------------
  --- Semantics for conditions
  --- ------------------------

  --- Laws for conjunction and disjunction (additional to ID in the constructor)
  ceq { T ; MEXT ; MIN ; False AND C1 } = { T ; MEXT ; MIN ;  False }
     if C1 =/= True .
  ceq { T ; MEXT ; MIN ; True OR   C1 } = { T ; MEXT ; MIN ;  True }
     if C1 =/= False .

  --- External events
  rl [EV-EXT] : { T ; MEXT ; MIN ; wait(event(str),n,m) AND  C1 }
         => { T ; MEXT ; MIN  ;  in(msg(str) , MEXT)   AND  C1 } .
  rl [EV-EXT] : { T ; MEXT ; MIN ; wait(event(str),n,m) OR   C1 }
         => { T ; MEXT ; MIN  ;  in(msg(str) , MEXT)   OR   C1 } .

  --- Checking if a (external) message is in a configuration
  op in : Msg Configuration -> Condition .
  --- Non -deterministic behavior for verification purposes
  rl [inRT] : in(M, Cnf) => True .
  rl [inRF] : in(M, Cnf) => False .

  --- Internal events
  crl [EV-INT] : { T ; MEXT ; MIN ; wait(WC, n, m) AND C1 }
       =>     { T ; MEXT ; MIN ; C1 }
       if check(WC, MIN, n , m, T) == True .

  crl [EV-INT] : { T ; MEXT ; MIN ; wait(WC,n, m) OR C1 }
       =>     { T ; MEXT ; MIN ; True }
       if check(WC, MIN, n , m, T) == True .

  --- --------------
  --- Rules for TOs
  --- --------------

  --- start rules
  crl [start] :
     < top : System | prog: (Sys < O1 : TO | start: C1, mstart: M1, ats >),
                      idle: (O1, SB),
                      run: RUN,
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
                      =>
     < top : System | prog: (Sys < O1 : TO | start: C1, mstart: M1, ats >),
                      idle: (SB, children-start(ats)),
                      run: (O1, RUN),
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: (PMSEG  start(O1, T) ),
                      in: IN,
                      out: (OUT  M1) ,
                      time: T >

     if { T ; IN ; PMSEG ; C1 } => { T ; IN ; PMSEG ; True } .

  crl [start] :
     < top : System | prog: (Sys < O1 : TO | start: C1, mstart: M1, ats >),
                      idle: (O1, SB),
                      run: RUN,
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
                      =>
     < top : System | prog: (Sys < O1 : TO | start: C1, mstart: M1, ats >),
                      idle: (SB),
                      run: RUN,
                      done: DONE,
                      cant-start: (NSTART, O1),
                      cant-stop:  NSTOP,
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
    if { T ; IN ; PMSEG ; C1 } => { T ; IN ; PMSEG ; False } .


  --- stop rules
  crl [stop] :
     < top : System | prog: (Sys < O1 : TO | stop: C1, mstop: M1, ats >),
                      idle: IDLE,
                      run: (O1, SB),
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
                      =>
     < top : System | prog: (Sys < O1 : TO | stop: C1, mstop: M1, ats >),
                      idle: IDLE,
                      run: (SB),
                      done: (O1, DONE),
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: (PMSEG  stop(O1, T) ),
                      in: IN,
                      out: (OUT  M1) ,
                      time: T >
     if { T ; IN ; PMSEG ; C1 } => { T ; IN ; PMSEG ; True } .

  crl [stop] :
     < top : System | prog: (Sys < O1 : TO | stop: C1, mstop: M1, ats >),
                      idle: IDLE,
                      run: (O1, SB),
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
                      =>
     < top : System | prog: (Sys < O1 : TO | stop: C1, mstop: M1, ats >),
                      idle: IDLE,
                      run: SB,
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  (O1, NSTOP),
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
     if { T ; IN ; PMSEG ; C1 } => { T ; IN ; PMSEG ; False } .

  --- ------------
  --- Time passing
  --- ------------
  rl [tick] :
     < top : System | prog: Sys,
                      idle: IDLE ,
                      run: RUN,
                      done: DONE,
                      cant-start: NSTART,
                      cant-stop:  NSTOP,
                      pmsg: PMSEG,
                      in: IN,
                      out: OUT,
                      time: T >
                      =>
     < top : System | prog: Sys,
                      idle: (IDLE, NSTART) ,
                      run:  (RUN, NSTOP),
                      done: DONE,
                      cant-start: empty,
                      cant-stop:  empty,
                      pmsg: PMSEG,
                      in: none,
                      out: none,
                      time: (T + 1) >
    .

  --- ---------------------
  --- Auxiliary definitions
  --- ---------------------

  --- Checking if an internal message is present
  op check : WaitCondition  Configuration Nat Nat Nat -> Condition .
  ceq check(start(O1), (start(O1, T') Cnf), n, m, T)
      = True
      if n + T' <= T and-then ( m == inf or-else T <= m + T') .
  ceq check(stop(O1), (stop(O1, T') Cnf), n, m, T)
      = True
      if n + T' <= T and-then ( m == inf or-else T <= m + T') .
  eq check(WC, Cnf,n , m , T) = False [owise] .

  --- Set of children that become active when the parent starts
  op children-start : AttributeSet -> TOSet .
  eq children-start( (children: TOs,  ats ) ) = TOs .   --- case for structures
  eq children-start( (guard,  c-if: TOs, c-else: TOs', ats ) ) = cond(TOs, TOs') . --- case for conditionals
  eq children-start(  ats  ) = empty .                  --- for all the other cases

  --- Choosing non-deterministically
  --- Note that the conditions is not really evaluated since it may depend
  --- on external values as the position of the mouse
  op cond : TOSet TOSet -> TOSet .
  rl [cond] : cond(TOs, TOs') => TOs .
  rl [cond] : cond(TOs, TOs') => TOs' .

  --- Making systems
  op make-system : Oid Configuration -> Object .
  eq make-system(O1, Sys) =
    < top : System | prog: Sys,
                    idle: O1,
                    run: empty,
                    done: empty,
                    cant-start: empty,
                    cant-stop:  empty,
                    pmsg: none,
                    in: none,
                    out: none,
                    time: 0
                    > .
endm
