mod SYNTAX is

  pr NAT .
  pr CONFIGURATION .

  --- Set of Qids
  pr SET{Qid}  *  ( sort Set{Qid}    to TOSet  ) .

  --- Identifiers for objects
  subsort Qid < Oid .

  --- Infinity for intervals
  op inf :       -> Nat [ctor] .

  --- Identifiers for temporal objects
  op TO :         -> Cid [ctor] .

  --- --------------------------------
  --- Attributes for temporal objects
  --- --------------------------------
  op start:_ : Condition                    -> Attribute [ctor] .
  op stop:_ : Condition                     -> Attribute [ctor] .
  op mstart:_ : Msg                         -> Attribute [ctor] .
  op mstop:_ : Msg                          -> Attribute [ctor] .
  op children:_ : TOSet                     -> Attribute [ctor] .
  --- For verification, the content of the condition is irrelevant
  op guard :                            -> Attribute [ctor] .
  op c-if:_   : TOSet                       -> Attribute [ctor] .
  op c-else:_ : TOSet                       -> Attribute [ctor] .

  --- ---------
  --- Messages
  --- ---------
  op msg : String                           -> Msg [ctor] .

  --- -----------
  --- Conditions
  --- -----------
  sort BCondition WaitCondition Condition .
  subsort BCondition < Condition .

  var WC                        : WaitCondition .
  var n                         : Nat .

  ops True False : -> BCondition .
  op _AND_ : Condition Condition            -> Condition [ctor assoc comm id: True ] .
  op _OR_ : Condition Condition             -> Condition [ctor assoc comm  id: False] .

  --- conditions on internal and external events
  op wait : WaitCondition Nat Nat           -> Condition .
  op wait : WaitCondition Nat               -> Condition .
  op wait : WaitCondition                   -> Condition .
  eq wait(WC) = wait(WC, 0, inf) .
  eq wait(WC, n) = wait(WC, n, n) .

  --- an external/internal event
  op event : String                      -> WaitCondition [ctor] .
  op start : Oid                         -> WaitCondition [ctor] .
  op stop  : Oid                         -> WaitCondition [ctor] .

  --- ---------------------
  --- Auxiliary definitions
  --- ---------------------
  vars O1        : Oid .
  vars C1 C2     : Condition .
  vars M1 M2     : Msg .
  vars TOs TOs'  : TOSet .

  --- Making TOs
  op make-texture : Oid Condition Condition Msg Msg -> Object .
  eq make-texture(O1, C1, C2, M1, M2) =
      < O1 : TO |      start: C1,
                       stop: C2,
                       mstart: M1,
                       mstop: M2
                       > .

  op make-structure : Oid TOSet Condition Condition -> Object .
  eq make-structure(O1, TOs, C1, C2) =
      < O1 : TO |      children: TOs,
                       start: C1,
                       stop: C2,
                       mstart: msg(""),
                       mstop: msg("")
                       > .

  op make-conditional : Oid TOSet TOSet Condition -> Object .
  op make-conditional : Oid TOSet Condition -> Object .
  eq make-conditional(O1, TOs, TOs', C1) =
       < O1 : TO |     guard,
                       c-if:   TOs,
                       c-else: TOs',
                       start:  C1,
                       stop:   C1,
                       mstart: msg(""),
                       mstop:  msg("")
                       > .
  eq make-conditional(O1, TOs, C1) = make-conditional(O1, TOs, empty, C1) .
endm
