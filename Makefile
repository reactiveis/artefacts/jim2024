ANTLR = antlr
PYTHON = python3

SRC_DIRS := ./examples
RIS_FILES := $(shell find $(SRC_DIRS) -name '*.is')
MAUDE_FILES := $(RIS_FILES:.is=.maude)

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: compile tests             ## Run all the tasks (compile & tests)

tests: $(MAUDE_FILES) .        ## Generate maude files from all the examples

compile: ReactiveIS.g4         ## Compile the grammar
	$(ANTLR) -Dlanguage=Python3 -listener -visitor -o dist $<

%.maude: %.is compile
	$(PYTHON) ./IS2Maude.py --input $< --output $@

clean:                        ## Cleanup project files
	rm -rf dist

.PHONY: help clean compile tests